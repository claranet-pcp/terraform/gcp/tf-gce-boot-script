output "object_md5" {
  value = "${google_storage_bucket_object.object.md5hash}"
}
output "bucket_link" {
  value = "${google_storage_bucket.bucket.self_link}"
}
