resource "google_storage_bucket_object" "object" {
  name   = "boot-script.sh"
  bucket = "${google_storage_bucket.bucket.name}"
  source = "${var.object}"
}

resource "google_storage_bucket" "bucket" {
  name     = "${var.bootstrap_bucket}"
  location = "${var.location}"
}

resource "google_storage_bucket_acl" "acl" {
  bucket = "${google_storage_bucket.bucket.name}"
}
